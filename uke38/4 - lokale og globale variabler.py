'''globale og lokale variabler'''

# lag en global variabel
tall = 5

# lag en funksjon som skal endre på den globale variabelen
# lag også en lokal variabel her
def endre():
    global tall
    tall = 2
    print(tall)
    global lokal
    lokal_tall = 16

# print ut den globale variabelen etter du har kjørt funksjonen
endre()
print(tall)

#prøv å printe ut den lokale variabelen
print(lokal_tall)


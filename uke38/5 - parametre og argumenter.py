'''parametre og argumenter'''

# Lag en funksjon som regner ut gjennomsnittetsalderen på et antall mennesker.
# Brukeren skal skrive inn alderen for det antallet mennesker, og det legges til
# en totalverdi. Skriv ut gjennomsnittet.

def average(antall):
    total = 0
    for i in range(antall):
        alder = int(input('Alder: '))
        total += alder
    print(f'Gjennomsnittet blir: {total/antall:.1f}')

average(4)
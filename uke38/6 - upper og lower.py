'''upper og lower'''

# Sjekk om en streng inneholder en av bokstavene 'æ', 'ø' eller 'å'
# print ut 'JA' dersom den gjør det

streng = 'Norsk setning med Æ'
ny = 'English without the letters'

def sjekk(tekst):
    for bokstav in tekst:
        if bokstav.lower() in 'æøå':
            print('JA')
sjekk(streng)
sjekk(ny)
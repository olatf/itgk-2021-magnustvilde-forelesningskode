'''
Oppsummering uke 38
'''

# break og continue
for i in range(1,11):
    if i == 6:
        print('Hopper over')
        continue
    elif i == 8:
        print('Avslutter løkken')
        break

# funksjoner
def min_funksjon():
    print('Min funksjonskode')

#funksjonskall
min_funksjon()

# lokale og globale variabler
glob = 'En global variabel'
def funk():
    lok = 'En lokal variabel'
    glob = 'Dette endrer IKKE verdien til den globale variabelen utenfor funksjonen'
    global glob
    glob = 'Nå kan vi endre den globale variabelen!'


#funksjon med parametre
def legg_sammen(x, y):
    print(f'{x+y}')

# argumenter
legg_sammen(15,6)

# upper() og lower()
streng = 'Hello World!'
print(streng.upper())
print(streng.lower())
